<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Pweb-UTS</title>
		<link rel="stylesheet" type="text/css" href="uas.css">
		<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@500&display=swap" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	</head>
	<body>
		<header>
			<div class="container">
				<h1><a href="">UNIVERSITAS AHMAD DAHLAN</a></h1>
			</div>
		</header>
		<section class="banner">
			<div class="container">
				<div class="banner-left">
					<img src="foto.jpg">
					<h2>Halooo...<br></h2>
						<h2><marquee>WELCOME TO MUHAMAD ANDHIKA SATYA AZIZI WEBSITE</marquee></h2>
					<p>Selamat Datang di Website Muhamad Andhika Satya Azizi!</p> <span class="efek-ngetik2"></span>
				</div>
			</div>
		</section>
		<section id="tentang">
			<div class="container">
				<h3>INPUT BIODATA KAMU</h3>
					<form method="post" action="proses_bio.php">
						<td width="150%" colspan="2">
							<td width="50%"><PRE>
								<center>
									<table>
										<tr>
											<td>Nama Lengkap</td>
											<td>:</td>
											<td><input type="text" name="pnama" size="40" ></td>
										</tr>
										<tr>
											<td>NIM</td>
											<td>:</td>
											<td><input type="number" name="pnim" size="40" ></td>
										</tr>
										<tr>
											<td>Jenis Kelamin</td>
											<td>:</td>
											<td><input type="text" name="pjenis_kelamin" size="40" ></td>
										</tr>
										<tr>
											<td>Prodi</td>
											<td>:</td>
											<td><input type="text" name="pprodi" size="40"></td>
										</tr>
										<tr>
											<td>Alamat</td>
											<td>:</td>
											<td><input type="text" name="palamat" size="40"></td>
										</tr>
										<tr>
											<td>Pesan</td>
											<td>:</td>
											<td><textarea rows="5" cols="20" name="ppesan"></textarea></td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td>
												<button type="submit" name="kirim">Kirim</button>
											</td>
										</tr>
									</table>
									<div align="center"><strong><a href="lihat_bio.php">::Lihat Data Kamu::</a></strong></div>
								</center>
							</PRE></td>
						</td>
					</table>
				</form>
			</div>
		</section>
		<footer>
			<div class="container">
				<small>Copyright &copy; 2022 - Muhamad Andhika Satya Azizi</small>
			</div>
		</footer>
	</body>
</html>